/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author JUAN DIEGO
 */
public class Main extends Application {

    private static Scene escena;
    private static Stage escenario;

    public void start(Stage primaryStage) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/vista/VistaPrincipal.fxml"));
            Pane ventana = (Pane) loader.load();

            escena = new Scene(ventana);
            primaryStage.setScene(escena);
            primaryStage.setTitle("Modo de juego");
            primaryStage.show();
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void cambiarVentana(String fileName) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource(fileName));
            Pane ventana = (Pane) loader.load();

            escena.setRoot(ventana);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Stage getEscenario() {
        return escenario;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
