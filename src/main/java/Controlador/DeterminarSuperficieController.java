/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import Modelo.QuadraticSurface;

/**
 * FXML Controller class
 *
 * @author JUAN DIEGO
 */
public class DeterminarSuperficieController implements Initializable {

    @FXML
    private TextField SuperficieA;

    @FXML
    private TextField SuperficieB;

    @FXML
    private TextField SuperficieC;

    @FXML
    private TextField SuperficieD;

    @FXML
    private TextField SuperficieE;

    @FXML
    private TextField SuperficieF;

    @FXML
    private TextField SuperficieG;

    @FXML
    private Button btnIdentificar;

    @FXML
    private ImageView btnRegresar;

    @FXML
    private Button btnReset;

    @FXML
    private TextArea txtAreaResultado;

    double a;
    double b;
    double c;
    double d;
    double e;
    double f;
    double g;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SuperficieA.setText("0");
        SuperficieB.setText("0");
        SuperficieC.setText("0");
        SuperficieD.setText("0");
        SuperficieE.setText("0");
        SuperficieF.setText("0");
        SuperficieG.setText("0");
    }

    @FXML
    void identificar(ActionEvent event) {
        txtAreaResultado.setText("");
        try {
            if(SuperficieA!=null){
            a = Double.parseDouble(SuperficieA.getText());
            } else{a=0;}
            if(SuperficieB!=null){
            b = Double.parseDouble(SuperficieB.getText());
            } else{b=0;}
            if(SuperficieC!=null){
            c = Double.parseDouble(SuperficieC.getText());
            } else{c=0;}
            if(SuperficieD!=null){
            d = Double.parseDouble(SuperficieD.getText());
            } else{d=0;}
            if(SuperficieE!=null){
            e = Double.parseDouble(SuperficieE.getText());
            } else{e=0;}
            if(SuperficieF!=null){
            f = Double.parseDouble(SuperficieF.getText());
            } else{f=0;}
            if(SuperficieG!=null){
            g = Double.parseDouble(SuperficieG.getText());
            } else{g=0;}
            QuadraticSurface sup = new QuadraticSurface(a, b, c, d, e, f, g);
            txtAreaResultado.appendText(sup.determineType());
        } 
        catch (NumberFormatException c) {
            c.getMessage();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Revisar Entradas no pueden haber letras ni espacios vacios");
            Optional<ButtonType> action = alert.showAndWait();
        } 
    }

    @FXML
    void regresar(MouseEvent event) {
        String fileName = "/vista/VistaPrincipal.fxml";
        Main.cambiarVentana(fileName);
    }

    @FXML
    void reset(ActionEvent event) {
        SuperficieA.setText("0");
        SuperficieB.setText("0");
        SuperficieC.setText("0");
        SuperficieD.setText("0");
        SuperficieE.setText("0");
        SuperficieF.setText("0");
        SuperficieG.setText("0");
        txtAreaResultado.setText("");
    }
}
