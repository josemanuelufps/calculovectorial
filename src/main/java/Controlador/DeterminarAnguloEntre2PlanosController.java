/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Calculos;
import Modelo.Line;
import Modelo.Plane;
import Modelo.Point;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import Modelo.LinesPlotter;

/**
 * teo odio nelson
 *
 * @author JUAN DIEGO
 */
public class DeterminarAnguloEntre2PlanosController implements Initializable {

    @FXML
    private Button btnCalcular;

    @FXML
    private Button btnReset;

    @FXML
    private ImageView btnReturn;

    @FXML
    private TextArea textAreaResultado;

    @FXML
    private TextField txtDistancia1AnguloPlano;

    @FXML
    private TextField txtDistancia2AnguloPlano;

    @FXML
    private TextField txtX1AnguloPlano;

    @FXML
    private TextField txtX2AnguloPlano;

    @FXML
    private TextField txtY1AnguloPlano;

    @FXML
    private TextField txtY2AnguloPlano;

    @FXML
    private TextField txtZ1AnguloPlano;

    @FXML
    private TextField txtZ2AnguloPlano;

    double distancia1;
    double distancia2;
    double x1;
    double x2;
    double y1;
    double y2;
    double z1;
    double z2;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    void calcular(ActionEvent event) {
        textAreaResultado.setText("");
        try{distancia1 = Double.parseDouble(txtDistancia1AnguloPlano.getText());
        distancia2 = Double.parseDouble(txtDistancia2AnguloPlano.getText());
        x1 = Double.parseDouble(txtX1AnguloPlano.getText());
        x2 = Double.parseDouble(txtX2AnguloPlano.getText());
        y1 = Double.parseDouble(txtY1AnguloPlano.getText());
        y2 = Double.parseDouble(txtY2AnguloPlano.getText());
        z1 = Double.parseDouble(txtZ1AnguloPlano.getText());
        z2 = Double.parseDouble(txtZ2AnguloPlano.getText());
        }catch (Exception e) {
            e.getMessage();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Error Digitacion");
            Optional<ButtonType> action = alert.showAndWait();
        }
        Plane plano = new Plane(new Point(x1,y1,z1),distancia1);
        Plane plano2 = new Plane(new Point(x2,y2,z2),distancia2);
        textAreaResultado.appendText(String.valueOf(Calculos.angleBetweenPlanes(plano,plano2)));
    }

    @FXML
    void regresar(MouseEvent event) {
        String fileName = "/vista/DeterminarAnguloPrincipal.fxml";
        Main.cambiarVentana(fileName);
    }

    @FXML
    void reset(ActionEvent event) {
        textAreaResultado.setText("");
        txtDistancia1AnguloPlano.setText("");
        txtDistancia2AnguloPlano.setText("");
        txtX1AnguloPlano.setText("");
        txtX2AnguloPlano.setText("");
        txtY1AnguloPlano.setText("");
        txtY2AnguloPlano.setText("");
        txtZ1AnguloPlano.setText("");
        txtZ2AnguloPlano.setText("");

    }

}
