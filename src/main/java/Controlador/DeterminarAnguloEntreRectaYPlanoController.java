/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Calculos;
import Modelo.Line;
import Modelo.Plane;
import Modelo.Point;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author JUAN DIEGO
 */
public class DeterminarAnguloEntreRectaYPlanoController implements Initializable {

    @FXML
    private TextField X1Recta;

    @FXML
    private TextField X2Recta;

    @FXML
    private TextField XPlano;

    @FXML
    private TextField Y1Recta;

    @FXML
    private TextField Y2Recta;

    @FXML
    private TextField YPlano;

    @FXML
    private TextField Z1Recta;

    @FXML
    private TextField Z2Recta;

    @FXML
    private TextField ZPlano;

    @FXML
    private Button btnCalcular;

    @FXML
    private ImageView btnRegresar;

    @FXML
    private Button btnReset;

    @FXML
    private TextField distanciasPlano;

    @FXML
    private TextArea txtAreaResultados;

    double distancia;
    double x1;
    double x2;
    double x;
    double y1;
    double y2;
    double y;
    double z1;
    double z2;
    double z;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void calcular(ActionEvent event) {
        txtAreaResultados.setText("");
        try {
             distancia = Double.parseDouble(distanciasPlano.getText());
             x1 = Double.parseDouble(X1Recta.getText());
             x2 = Double.parseDouble(X2Recta.getText());
             x = Double.parseDouble(XPlano.getText());
             y1 = Double.parseDouble(Y1Recta.getText());
             y2 = Double.parseDouble(Y2Recta.getText());
             y = Double.parseDouble(YPlano.getText());
             z1 = Double.parseDouble(Z1Recta.getText());
             z2 = Double.parseDouble(Z2Recta.getText());
             z = Double.parseDouble(ZPlano.getText());
        } catch (Exception e) {
            e.getMessage();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Error Digitacion");
            Optional<ButtonType> action = alert.showAndWait();
        }
        Line linea1 = new Line(new Point(x1,y1,z1),new Point(x2,y2,z2));
        Plane plano = new Plane(new Point(x,y,z),distancia);
        txtAreaResultados.appendText(String.valueOf(Calculos.angleBetweenLineAndPlane(linea1,plano)));
    }

    @FXML
    void regresar(MouseEvent event) {
        String fileName = "/vista/DeterminarAnguloPrincipal.fxml";
        Main.cambiarVentana(fileName);
    }

    @FXML
    void reset(ActionEvent event) {
        X1Recta.setText("");
        X2Recta.setText("");
        XPlano.setText("");
        Y1Recta.setText("");
        Y2Recta.setText("");
        YPlano.setText("");
        Z1Recta.setText("");
        Z2Recta.setText("");
        ZPlano.setText("");
        distanciasPlano.setText("");
        txtAreaResultados.setText("");
    }
}
