/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author JUAN DIEGO
 */
public class DeterminarAnguloPrincipalController implements Initializable {
   
     @FXML
    private Label btn2Planos;

    @FXML
    private Label btn2Rectas;

    @FXML
    private Label btnRectaPlano;

    @FXML
    private ImageView btnRegresar;
    
    @FXML
    private Label btnSobreNosotros;
    
    @Override
        public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
        
     @FXML
    void entre2Planos(MouseEvent event) {
        String fileName = "/vista/DeterminarAnguloEntre2planos.fxml";
        Main.cambiarVentana(fileName);
    }

    @FXML
    void entre2Rectas(MouseEvent event) {
        String fileName = "/vista/DeterminarAnguloEntre2rectas.fxml";
        Main.cambiarVentana(fileName);
    }

    @FXML
    void entreRectasyPlano(MouseEvent event) {
        String fileName = "/vista/DeterminarAnguloEntrerectayplano.fxml";
        Main.cambiarVentana(fileName);
    }

    @FXML
    void regresar(MouseEvent event) {
        String fileName = "/vista/VistaPrincipal.fxml";
        Main.cambiarVentana(fileName);
    }
    
    @FXML
    void sobreNosotros(MouseEvent event) {
        String fileName = "/vista/SobreNosotros.fxml";
        Main.cambiarVentana(fileName);
    }
}
