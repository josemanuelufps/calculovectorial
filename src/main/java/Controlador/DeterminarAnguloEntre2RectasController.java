/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Calculos;
import static Modelo.Calculos.angleBetweenLines;
import Modelo.Line;
import Modelo.Point;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import Modelo.LinesPlotter;

/**
 * FXML Controller class
 *
 * @author JUAN DIEGO
 */
public class DeterminarAnguloEntre2RectasController implements Initializable {

    @FXML
    private TextField R1X1;

    @FXML
    private TextField R1X2;

    @FXML
    private TextField R1Y1;

    @FXML
    private TextField R1Y2;

    @FXML
    private TextField R1Z1;

    @FXML
    private TextField R1Z2;

    @FXML
    private TextField R2X1;

    @FXML
    private TextField R2X2;

    @FXML
    private TextField R2Y1;

    @FXML
    private TextField R2Y2;

    @FXML
    private TextField R2Z1;

    @FXML
    private TextField R2Z2;

    @FXML
    private Button btnCalcular;

    @FXML
    private Button btnReste;

    @FXML
    private TextArea txtAreaResultados;
    
    double r1x1;
            double r1x2;
            double r1y1;
            double r1y2;
            double r1z1;
            double r1z2;
            double r2x1;
            double r2x2;
            double r2y1;
            double r2y2;
            double r2z1;
            double r2z2;
            
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void calcular(ActionEvent event) {
        txtAreaResultados.setText("");
        try {
             r1x1 = Double.parseDouble(R1X1.getText());
             r1x2 = Double.parseDouble(R1X2.getText());
             r1y1 = Double.parseDouble(R1Y1.getText());
             r1y2 = Double.parseDouble(R1Y2.getText());
             r1z1 = Double.parseDouble(R1Z1.getText());
             r1z2 = Double.parseDouble(R1Z2.getText());
             r2x1 = Double.parseDouble(R2X1.getText());
             r2x2 = Double.parseDouble(R2X2.getText());
             r2y1 = Double.parseDouble(R2Y1.getText());
             r2y2 = Double.parseDouble(R2Y2.getText());
             r2z1 = Double.parseDouble(R2Z1.getText());
             r2z2 = Double.parseDouble(R2Z2.getText());
        } catch (Exception e) {
            e.getMessage();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("Error Digitacion");
            Optional<ButtonType> action = alert.showAndWait();
        }
        Point punto1 = new Point(r1x1,r1y1,r1z1);
        Point punto2 = new Point(r1x2,r1y2,r1z2);
        Point punto3 = new Point(r2x1,r2y1,r2z1);
        Point punto4 = new Point(r2x2,r2y2,r2z2);
        LinesPlotter linea = new LinesPlotter(new Line(punto1,punto2),new Line(punto3,punto4));
        txtAreaResultados.appendText(String.valueOf(Calculos.angleBetweenLines(new Line(punto1,punto2),new Line(punto3,punto4))));
    }

    @FXML
    void regresar(MouseEvent event) {
        String fileName = "/vista/DeterminarAnguloPrincipal.fxml";
        Main.cambiarVentana(fileName);
    }

    @FXML
    void reset(ActionEvent event) {
        R1X1.setText("");
        R1X2.setText("");
        R1Y1.setText("");
        R1Y2.setText("");
        R1Z1.setText("");
        R1Z2.setText("");
        R2X1.setText("");
        R2X2.setText("");
        R2Y1.setText("");
        R2Y2.setText("");
        R2Z1.setText("");
        R2Z2.setText("");
        txtAreaResultados.setText("");
    }

}
