/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author JUAN DIEGO
 */
public class VistaPrincipalController implements Initializable {

    @FXML
    private Label btnDterminarSuperficie;
    @FXML
    private Label btnDeterminariAngulo;
    @FXML
    private Label btnSobreNosotros;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void determinarSuperfice(MouseEvent event) {
        String fileName = "/vista/DeterminarSuperficie.fxml";
        Main.cambiarVentana(fileName);
    }

    @FXML
    private void determinariAngulo(MouseEvent event) {
        String fileName = "/vista/DeterminarAnguloPrincipal.fxml";
        Main.cambiarVentana(fileName);
    }

    @FXML
    private void sobreNosotros(MouseEvent event) {
        String fileName = "/vista/SobreNosotros.fxml";
        Main.cambiarVentana(fileName);
    }

}
