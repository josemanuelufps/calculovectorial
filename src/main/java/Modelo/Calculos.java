package Modelo;

public class Calculos {
    public static double angleBetweenLines(Line line1, Line line2) {
        Point direction1 = new Point(line1.getEnd().getX() - line1.getStart().getX(), line1.getEnd().getY() - line1.getStart().getY(), line1.getEnd().getZ() - line1.getStart().getZ());
        Point direction2 = new Point(line2.getEnd().getX() - line2.getStart().getX(), line2.getEnd().getY() - line2.getStart().getY(), line2.getEnd().getZ() - line2.getStart().getZ());

        double dotProduct = direction1.getX() * direction2.getX() + direction1.getY() * direction2.getY() + direction1.getZ() * direction2.getZ();
        double magnitude1 = Math.sqrt(Math.pow(direction1.getX(), 2) + Math.pow(direction1.getY(), 2) + Math.pow(direction1.getZ(), 2));
        double magnitude2 = Math.sqrt(Math.pow(direction2.getX(), 2) + Math.pow(direction2.getY(), 2) + Math.pow(direction2.getZ(), 2));

        return Math.toDegrees(Math.acos(dotProduct / (magnitude1 * magnitude2)));
    }

    public static double angleBetweenPlanes(Plane plane1, Plane plane2) {
        double dotProduct = plane1.getNormal().getX() * plane2.getNormal().getX() + plane1.getNormal().getY() * plane2.getNormal().getY() + plane1.getNormal().getZ() * plane2.getNormal().getZ();
        double magnitude1 = Math.sqrt(Math.pow(plane1.getNormal().getX(), 2) + Math.pow(plane1.getNormal().getY(), 2) + Math.pow(plane1.getNormal().getZ(), 2));
        double magnitude2 = Math.sqrt(Math.pow(plane2.getNormal().getX(), 2) + Math.pow(plane2.getNormal().getY(), 2) + Math.pow(plane2.getNormal().getZ(), 2));

        return Math.toDegrees(Math.acos(dotProduct / (magnitude1 * magnitude2)));
    }

    public static double angleBetweenLineAndPlane(Line line, Plane plane) {
        Point direction = new Point(line.getEnd().getX() - line.getStart().getX(), line.getEnd().getY() - line.getStart().getY(), line.getEnd().getZ() - line.getStart().getZ());

        double dotProduct = direction.getX() * plane.getNormal().getX() + direction.getY() * plane.getNormal().getY() + direction.getZ() * plane.getNormal().getZ();
        double magnitude1 = Math.sqrt(Math.pow(direction.getX(), 2) + Math.pow(direction.getY(), 2) + Math.pow(direction.getZ(), 2));
        double magnitude2 = Math.sqrt(Math.pow(plane.getNormal().getX(), 2) + Math.pow(plane.getNormal().getY(), 2) + Math.pow(plane.getNormal().getZ(), 2));

        return Math.toDegrees(Math.acos(dotProduct / (magnitude1 * magnitude2)));
    }
}