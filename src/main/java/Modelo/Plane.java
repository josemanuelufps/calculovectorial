package Modelo;
public class Plane {
    private Point normal;
    private double distance;

    public Plane(Point normal, double distance) {
        this.normal = normal;
        this.distance = distance;
    }

    public Point getNormal() {
        return normal;
    }

    public double getDistance() {
        return distance;
    }

    public void setNormal(Point normal) {
        this.normal = normal;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
