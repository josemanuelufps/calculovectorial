/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Modelo.Plane;
import Modelo.Line;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author anorak
 */
public class LinePlane {
    private Plane plane;
    private Line line;

    public void clear() {
        this.plane = null;
        this.line = null;
    }

    public LinePlane(Line line, Plane plane) {
        this.plane = plane;
        this.line = line;
    }

    public void generarScriptOctave(String nombreArchivo) {

        String ruta = System.getProperty("user.home") + "/Desktop/";

        String rutaCompleta = ruta + nombreArchivo;

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(rutaCompleta));
            
            double a = plane.getNormal().getX();
            double b = plane.getNormal().getY();
            double c = plane.getNormal().getZ();
            double d = plane.getDistance();

            double xStart = line.getStart().getX();
            double yStart = line.getStart().getY();
            double zStart = line.getStart().getZ();

            double xEnd = line.getEnd().getX();
            double yEnd = line.getEnd().getY();
            double zEnd = line.getEnd().getZ();
            
            writer.write("[x, y] = meshgrid(-10:0.5:10, -10:0.5:10);\n");
            
            writer.write(String.format("plane=[%f,%f,%f,%f];\n", a, b, c, d));
            writer.write("z=((-plane(1)*x - plane(2)*y - plane(4)) / plane(3));\n");
            
            writer.write(String.format("start=[%f,%f,%f];\n", xStart, yStart, zStart));
            writer.write(String.format("end_p=[%f,%f,%f];\n", xEnd, yEnd, zEnd));
            

            writer.write("figure;\n"
                    + "xlabel('X-axis');\n"
                    + "ylabel('Y-axis');\n"
                    + "zlabel('Z-axis');\n"
                    + "title('3D Plot linea y plano');\n"
                    + "grid on;\n"
                    + "axis equal;\n"
                    + "hold on;\n");

            
            writer.write("surf(x, y, z, 'FaceColor', 'b', 'FaceAlpha', 0.6, 'EdgeColor', 'none');\n");
            writer.write("plot3([start(1), end_p(1)], [start(2), end_p(2)], [start(3), end_p(3)], 'r-', 'LineWidth', 2);\n");

            writer.write("hold off;");
            writer.close();
            System.out.println("Archivo escrito exitosamente");

        } catch (IOException e) {
            System.err.println("Error al escribir en el archivo: " + e.getMessage());
        }
    }
}
