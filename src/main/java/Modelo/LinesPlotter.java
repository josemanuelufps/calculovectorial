package Modelo;

import Modelo.Line;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class LinesPlotter {

    private ArrayList<Line> lines;

    public void addLine(Line line) {
        lines.add(line);
    }

    public void clear() {
        lines.clear();
    }

    public LinesPlotter() {
        lines = new ArrayList<>();
    }

    public LinesPlotter(Line linea1, Line linea2) {
        lines = new ArrayList<>();
        this.lines.add(linea1);
        this.lines.add(linea2);
    }

    public void generarScriptOctave(String nombreArchivo) {

        String ruta = System.getProperty("user.home") + "/Desktop/";

        String rutaCompleta = ruta + nombreArchivo;

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(rutaCompleta));

            for (int i = 0; i < this.lines.size(); i++) {
                double xStart = lines.get(i).getStart().getX();
                double yStart = lines.get(i).getStart().getY();
                double zStart = lines.get(i).getStart().getZ();

                double xEnd = lines.get(i).getEnd().getX();
                double yEnd = lines.get(i).getEnd().getY();
                double zEnd = lines.get(i).getEnd().getZ();

                writer.write(String.format("start%d=[%f,%f,%f];\n", i, xStart, yStart, zStart));
                writer.write(String.format("end%d=[%f,%f,%f];\n", i, xEnd, yEnd, zEnd));
            }

            writer.write("figure;\n"
                    + "xlabel('X-axis');\n"
                    + "ylabel('Y-axis');\n"
                    + "zlabel('Z-axis');\n"
                    + "title('3D Plot of Two Lines');\n"
                    + "grid on;\n"
                    + "axis equal;\n"
                    + "hold on;\n");

            for (int i = 0; i < this.lines.size(); i++) {
                writer.write(String.format("plot3([start%d(1), end%d(1)], [start%d(2), end%d(2)], [start%d(3), end%d(3)], '%c-', 'LineWidth', 2);\n",
                        i, i, i, i, i, i, i % 2 == 0 ? 'b' : 'r'));
            }

            for (int i = 0; i < this.lines.size(); i++) {
                writer.write(String.format("direction%d = end%d - start%d;", i, i, i));
            }

//            for (int i = 0; i < this.lines.size() - 1; i++) {
//                writer.write(String.format("angle%d%d = rad2deg(acos(dot(direction%d, direction%d) / (norm(direction%d) * norm(direction%d))));",
//                         i, i+1, i, i+1, i, i+1));
//            }

            writer.write("angles = rad2deg(acos(dot(direction1, direction2) / (norm(direction1) * norm(direction2))));");

            writer.write(("angle_label = sprintf('Angulo: %.2f grados', angles);"));
            writer.write("legend('Line 1', 'Line 2', angle_label, 'Location', 'best');");

            writer.write("hold off;");
            writer.close();
            System.out.println("Archivo escrito exitosamente");

        } catch (IOException e) {
            System.err.println("Error al escribir en el archivo: " + e.getMessage());
        }
    }
}
