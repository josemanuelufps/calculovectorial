package Modelo;

public class QuadraticSurface {
    private double A, B, C, D, E, F, G;
   private Ecuaciones eq= new Ecuaciones();
    public QuadraticSurface(double A, double B, double C, double D, double E, double F, double G) {
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;
        this.E = E;
        this.F = F;
        this.G = G;
        
    }

    public String determineType() {
        if (A == B && B == C && D == 0 && E == 0 && F == 0) {
             eq.crearArchivoEsfera(String.valueOf(A),String.valueOf(B),String.valueOf(C));
            return "Esfera";
        } else if (D == 0 && E == 0 && F == 0 && ((A > 0 && B > 0 && C > 0) || (A < 0 && B < 0 && C < 0))) {
             eq.crearArchivoElipsoide(String.valueOf(A),String.valueOf(B),String.valueOf(C));
            return "Elipsoide";
        } else if ((A > 0 && B > 0 && C < 0) || (A < 0 && B < 0 && C > 0)) {
            eq.crearArchivoHiperboloideDeUnaHoja(String.valueOf(A),String.valueOf(B),String.valueOf(C));
            return "Hiperboloide de una hoja";
        } else if ((A > 0 && B < 0 && C < 0) || (A < 0 && B > 0 && C > 0)) {
            eq.crearArchivoHiperboloideDeDosHojas(String.valueOf(A),String.valueOf(B),String.valueOf(C));
            return "Hiperboloide de dos hojas";
        } else if ((A > 0 && B == 0 && C == 0) || (A == 0 && B > 0 && C == 0) || (A == 0 && B == 0 && C > 0)) {
            eq.crearArchivoParaboloideEliptico(String.valueOf(A),String.valueOf(B),String.valueOf(C));
            return "Paraboloide elíptico";
        } else if ((A < 0 && B == 0 && C == 0) || (A == 0 && B < 0 && C == 0) || (A == 0 && B == 0 && C < 0)) {
            eq.crearArchivoParaboloideHiperbolico(String.valueOf(A),String.valueOf(B),String.valueOf(C));
            return "Paraboloide hiperbólico";
        }else if ((A != 0 && B != 0 && C == 0) || (A != 0 && B == 0 && C != 0) || (A == 0 && B != 0 && C != 0)) {
            eq.crearArchivoConoEliptico(String.valueOf(A),String.valueOf(B),String.valueOf(C));
            return "Cono elíptico";
        }  else {
            return "Superficie cuadrática general";
        }
    }


//Start GetterSetterExtension Source Code

    /**GET Method Propertie A*/
    public double getA(){
        return this.A;
    }//end method getA

    /**SET Method Propertie A*/
    public void setA(double A){
        this.A = A;
    }//end method setA

    /**GET Method Propertie B*/
    public double getB(){
        return this.B;
    }//end method getB

    /**SET Method Propertie B*/
    public void setB(double B){
        this.B = B;
    }//end method setB

    /**GET Method Propertie C*/
    public double getC(){
        return this.C;
    }//end method getC

    /**SET Method Propertie C*/
    public void setC(double C){
        this.C = C;
    }//end method setC

    /**GET Method Propertie D*/
    public double getD(){
        return this.D;
    }//end method getD

    /**SET Method Propertie D*/
    public void setD(double D){
        this.D = D;
    }//end method setD

    /**GET Method Propertie E*/
    public double getE(){
        return this.E;
    }//end method getE

    /**SET Method Propertie E*/
    public void setE(double E){
        this.E = E;
    }//end method setE

    /**GET Method Propertie F*/
    public double getF(){
        return this.F;
    }//end method getF

    /**SET Method Propertie F*/
    public void setF(double F){
        this.F = F;
    }//end method setF

    /**GET Method Propertie G*/
    public double getG(){
        return this.G;
    }//end method getG

    /**SET Method Propertie G*/
    public void setG(double G){
        this.G = G;
    }//end method setG

    /**GET Method Propertie eq*/
    public Ecuaciones getEq(){
        return this.eq;
    }//end method getEq

    /**SET Method Propertie eq*/
    public void setEq(Ecuaciones eq){
        this.eq = eq;
    }//end method setEq

//End GetterSetterExtension Source Code


}//End class