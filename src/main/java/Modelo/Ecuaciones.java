package Modelo;
        

import java.io.*;

public class Ecuaciones
{
    private String Ecuacion;
    Ecuaciones(){
        Ecuacion=null;
    }
    Ecuaciones(String ecuacion){
        this.Ecuacion=Ecuacion;
    }
    public void crearArchivoEsfera(String A,String B,String C){
        
        String octaveScript = "a = "+A+";\n" +
"b = "+B+";\n" +
"c = "+C+";\n" +
"\n" +
"theta = linspace(0, 2*pi, 100);\n" +
"phi = linspace(0, pi, 50);\n" +
"[theta, phi] = meshgrid(theta, phi);\n" +
"\n" +
"x = a * sin(phi) .* cos(theta);\n" +
"y = b * sin(phi) .* sin(theta);\n" +
"z = c * cos(phi);\n" +
"\n" +
"surf(x, y, z);\n" +
"axis equal;\n" +
" title('Esfera: Ecuación Canónica');\n" +
"\n" +
"xlabel(\"X\");\n" +
"ylabel(\"Y\");\n" +
"zlabel(\"Z\");";
escribirArchivo(octaveScript);
}
public void crearArchivoElipsoide(String A,String B,String C){
    
        String octaveScript =  "a = "+A+";\n" +
"b = "+B+";\n" +
"c = "+C+";\n" +
"theta = linspace(0, 2*pi, 100);\n" +
"phi = linspace(0, pi, 50);\n" +
"[theta, phi] = meshgrid(theta, phi);\n" +
"x = a * sin(phi) .* cos(theta);\n" +
"y = b * sin(phi) .* sin(theta);\n" +
"z = c * cos(phi);\n" +
"surf(x, y, z);\n" +
"axis equal;\n" +
"title('Elipsoide: Ecuación Canónica');\n" +
"xlabel(\"X\");\n" +
"ylabel(\"Y\");\n" +
"zlabel(\"Z\");";
escribirArchivo(octaveScript);
    

}

public void crearArchivoHiperboloideDeUnaHoja(String A,String B,String C){
   String octaveScript =   "a = "+A+";\n" +
"b = "+B+";\n" +
"c = "+C+";\n" +
"theta = linspace(0, 2*pi, 50);\n" +
"z = linspace(-1.5, 1.5, 50);\n" +
"[theta, z] = meshgrid(theta, z);\n" +
"x = a * cosh(z) .* cos(theta);\n" +
"y = b * cosh(z) .* sin(theta);\n" +
"z = c * sinh(z);\n"+
"surf(x, y, z);\n" +
"axis equal;\n" +
"title('Hiperboloide de una hoja: Ecuación Canónica');\n" +
"xlabel(\"X\");\n" +
"ylabel(\"Y\");\n" +
"zlabel(\"Z\");";
escribirArchivo(octaveScript);
}

public void crearArchivoHiperboloideDeDosHojas(String A,String B,String C){
   String octaveScript =  "% Definir las constantes\n" +
"a = "+A+";\n" +
"b = "+B+";\n" +
"c = "+C+";\n" +
"\n" +
"% Validar valores de a, b y c\n" +
"if a == 0\n" +
"  a = 1;\n" +
"end\n" +
"if b == 0\n" +
"  b = 1;\n" +
"end\n" +
"if c == 0\n" +
"  c = 1;\n" +
"end\n" +
"\n" +
"% Definir el rango de valores para u y v\n" +
"u = linspace(-2*pi, 2*pi, 50);\n" +
"v = linspace(-1.5, 1.5, 40);\n" +
"\n" +
"% Crear la malla\n" +
"[U, V] = meshgrid(u, v);\n" +
"\n" +
"% Calcular las coordenadas positivas y negativas para x\n" +
"x_pos = a * cosh(V);\n" +
"x_neg = -a * cosh(V);\n" +
"\n" +
"% Calculate y and z using the meshgrid (U, V)\n" +
"y = b * cos(U) .* sinh(V);\n" +
"z = c * sin(U) .* sinh(V);\n" +
"\n" +
"% Graficar ambas superficies\n" +
"figure;\n" +
"hold on;\n" +
"\n" +
"surf(x_pos, y, z);  % Positive side\n" +
"surf(x_neg, y, z);  % Negative side\n" +
"\n" +
"title('Hiperboloide de dos caras');\n" +
"xlabel('x');\n" +
"ylabel('y');\n" +
"zlabel('z');\n" +
"\n" +
"view(3);\n" +
"axis equal;\n" +
"camlight;";
escribirArchivo(octaveScript);
}
public void crearArchivoParaboloideEliptico(String A,String B,String C){
   String octaveScript =  "a = "+A+";\n" +
"b = "+B+";\n" +
"c = "+C+";\n" +
"\n" +
"% Definición de los valores de u y v\n" +
"u = linspace(0, 2*pi, 50);\n" +
"v = linspace(0, 1.5, 30);\n" +
"[u, v] = meshgrid(u, v);\n" +
"\n" +
"% Coordenadas del paraboloide elíptico\n" +
"if a > 0 && b == 0 && c == 0\n" +
"    x = a * v .* cos(u);\n" +
"    y = 1 * v .* sin(u);\n" +
"    z = v.^2 / 4;\n" +
"    title('Paraboloide Elíptico en X');\n" +
"elseif b > 0 && a == 0 && c == 0\n" +
"    x =1 * v .* cos(u);\n" +
"    y = b * v .* sin(u);\n" +
"    z = v.^2 / 4;\n" +
"    title('Paraboloide Elíptico en Y');\n" +
"elseif c > 0 && a == 0 && b == 0\n" +
"    x = 1 * v .* cos(u);\n" +
"    y = 1 * v .* sin(u);\n" +
"    z = c * v.^2 / 4;\n" +
"    title('Paraboloide Elíptico en Z');\n" +
"end\n" +
"\n" +

"% Graficar el paraboloide\n" +
"surf(x, y, z);\n" +
"title('Paraboloide Elíptico');\n"+
"xlabel('X');\n" +
"ylabel('Y');\n" +
"zlabel('Z');\n" +
"rotate3d;";
escribirArchivo(octaveScript);
}
public void crearArchivoParaboloideHiperbolico(String A,String B,String C){
   String octaveScript =  
"% Parámetros para el paraboloide hiperbólico\n" +
"a = 0;\n" +
"b = 0;\n" +
"c = 0;\n" +
"\n" +
"% Verificar si a, b y c son 0, en cuyo caso se establecen a 1\n" +
"if a == 0\n" +
"    a = 1;\n" +
"end\n" +
"if b == 0\n" +
"    b = 1;\n" +
"end\n" +
"if c == 0\n" +
"    c = 1;\n" +
"end\n" +
"\n" +
"% Rango de valores para u y v\n" +
"u = linspace(-2, 2, 100);\n" +
"v = linspace(-2, 2, 100);\n" +
"\n" +
"% Crear una malla de coordenadas\n" +
"[U, V] = meshgrid(u, v);\n" +
"\n" +
"% Definir las ecuaciones para X, Y, Z\n" +
"X = a * U;\n" +
"Y = b * V;\n" +
"Z = c * (X.^2 - Y.^2);\n" +
"\n" +
"% Graficar el paraboloide hiperbólico\n" +
"figure;\n" +
"surf(X, Y, Z);\n" +
"title('Paraboloide Hiperbólico');\n" +
"xlabel('X');\n" +
"ylabel('Y');\n" +
"zlabel('Z');";

escribirArchivo(octaveScript);
}
public void crearArchivoConoEliptico(String A,String B,String C){
   String octaveScript = "a = "+A+";\n" +
"b = "+B+";\n" +
"c = "+C+";\n" +
"\n" +
"% Verificar si alguna de las variables es cero y reemplazarla por uno\n" +
"if a == 0\n" +
"    a = 1.0;\n" +
"end\n" +
"if b == 0\n" +
"    b = 1.0;\n" +
"end\n" +
"if c == 0\n" +
"    c = 1.0;\n" +
"end\n" +
"\n" +
"% Crear mallas de valores u y v\n" +
"u = linspace(0, 2*pi, 50);\n" +
"v = linspace(-5, 5, 30);\n" +
"[u, v] = meshgrid(u, v);\n" +
"\n" +
"% Coordenadas (x, y, z) del cono elíptico\n" +
"x = a * v .* cos(u);\n" +
"y = b * v .* sin(u);\n" +
"z = c/2 * v;\n" +
"\n" +
"% Graficar el cono\n" +
"surf(x, y, z);\n" +
"title('Cono Elíptico');\n" +
"xlabel('X');\n" +
"ylabel('Y');\n" +
"zlabel('Z');\n" +
"rotate3d; % Habilitar la rotación 3D";
escribirArchivo(octaveScript);
}
public void escribirArchivo(String lectura){
  try {
            PrintWriter writer = new PrintWriter(new FileWriter("octave_script.m"));
            writer.println(lectura);
            writer.close();
            System.out.println(".m creado exitosamente.");
        } catch (IOException e) {
            System.err.println("Error al escribir el archivo: " + e.getMessage());
        }  
}

    }
    
