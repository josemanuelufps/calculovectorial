/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Modelo.Plane;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author anorak
 */
public class PlanesPlotter {

    private ArrayList<Plane> planes;

    public void addPlane(Plane line) {
        planes.add(line);
    }

    public void clear() {
        planes.clear();
    }

    public PlanesPlotter() {
        planes = new ArrayList<>();
    }

    public PlanesPlotter(Plane plane1, Plane plane2) {
        planes = new ArrayList<>();
        this.planes.add(plane1);
        this.planes.add(plane2);
    }

    public void generarScriptOctave(String nombreArchivo) {

        String ruta = System.getProperty("user.home") + "/Desktop/";

        String rutaCompleta = ruta + nombreArchivo;

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(rutaCompleta));

            writer.write("[x, y] = meshgrid(-10:0.5:10, -10:0.5:10);");

            for (int i = 0; i < this.planes.size(); i++) {
                double a = planes.get(i).getNormal().getX();
                double b = planes.get(i).getNormal().getY();
                double c = planes.get(i).getNormal().getZ();
                double d = planes.get(i).getDistance();

                writer.write(String.format("plane%d=[%f,%f,%f,%f];\n", i, a, b, c, d));
                writer.write(String.format("z%d=((-plane%d(1)*x - plane%d(2)*y - plane%d(4)) / plane%d(3));\n", i, i, i, i, i));
            }

            writer.write("figure;\n"
                    + "xlabel('X-axis');\n"
                    + "ylabel('Y-axis');\n"
                    + "zlabel('Z-axis');\n"
                    + "title('3D Plot of Planes');\n"
                    + "grid on;\n"
                    + "axis equal;\n"
                    + "hold on;\n");

            for (int i = 0; i < this.planes.size(); i++) {
                writer.write(String.format("surf(x, y, z%d, 'FaceColor', '%c', 'FaceAlpha', 0.6, 'EdgeColor', 'none');\n",
                        i, i % 2 == 0 ? 'b' : 'r'));
            }

            writer.write("hold off;");
            writer.close();
            System.out.println("Archivo escrito exitosamente");

        } catch (IOException e) {
            System.err.println("Error al escribir en el archivo: " + e.getMessage());
        }
    }
}
